#ifndef CCPATIENTRECORD_H
#define CCPATIENTRECORD_H

#include <QString>
#include <QStringList>
#include <QDateTime>
#include "ccconsultationrecord.h"

class CCPatientRecord
{
public:
    explicit CCPatientRecord(QString aFirstName, QString aLastName, int aOhipNumber,
                             QDate aDateOfBirth, int aPhysicianId, int aAreaCode = -1, int aStationCode = -1, int aDirectoryCode = -1,
                             QString aAddress1 = 0, QString aAddress2 = 0,
                             QString aProvince = 0, QString aCountry = 0, QString aPostalCode = 0, CCConsultationRecordStatus aPatientStatus = CCConsultationRecordStatusComplete);
    explicit CCPatientRecord(QString constructorString);
    ~CCPatientRecord();

    QString firstName;
    QString lastName;
    int ohipNumber;
    int physicianId;
    QDate dateOfBirth;
    int areaCode;
    int stationCode;
    int directoryCode;
    QString address1;
    QString address2;
    QString province;
    QString country;
    QString postalCode;
    CCConsultationRecordStatus patientStatus;
};

#endif // CCPATIENTRECORD_H
