#include "ccpatientrecordcontrol.h"
#include "ccutils.h"

CCPatientRecordControl* CCPatientRecordControlSingleton = 0;

CCPatientRecordControl::CCPatientRecordControl() :
    applyingFilter(false)
{
}

CCPatientRecordControl::~CCPatientRecordControl()
{

}

CCPatientRecordControl* CCPatientRecordControl::sharedPatientRecordControl()
{
    if (CCPatientRecordControlSingleton == 0)
    {
        CCPatientRecordControlSingleton = new CCPatientRecordControl();
    }

    return CCPatientRecordControlSingleton;
}

void CCPatientRecordControl::receiveServerData(QByteArray *received)
{
    QString constructorString = QString(*received);

    qDebug() << constructorString;

    if (constructorString.contains("SERVER_CONNECTION_CONFIRM"))
    {
        // Server connection completed
        return;
    }
    else if (constructorString.contains("MESSAGE_DUMP_END"))
    {
        // Clinic list completely returned
        _awaitingServerResponse = false;
        applyingFilter = false;
        _socket->disconnectFromServer();
    }
    else if (constructorString.contains("FILTER"))
    {
        applyingFilter = true;
        _filteredPatients.clear();
    }
    else if (applyingFilter)
    {
        // Parse through and set as QVector<int>
        QStringList parseString;
        parseString = constructorString.split(",");

        for (int i = 0; i < parseString.count(); i++)
        {
            _filteredPatients.append(parseString.at(i).toInt());
        }
    }
    else
    {
        CCPatientRecord* newRecord = new CCPatientRecord(constructorString);
        _patientRecordsMap.insert(newRecord->ohipNumber, newRecord);
    }
}

CCPatientRecord* CCPatientRecordControl::getPatientRecordForId(int aPatientRecordId)
{
    if (_patientRecordsMap.size() < 1 || (!_patientRecordsMap.contains(aPatientRecordId)))
    {
        this->refreshPatientRecords();
    }

    if (_patientRecordsMap.contains(aPatientRecordId))
    {
        return _patientRecordsMap[aPatientRecordId];
    }

    return 0;
}

void CCPatientRecordControl::refreshPatientRecords()
{
    _patientRecordsMap.clear();

    // call server and refresh map
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }

    _socket->connectToServer();
    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 91;//90;
    serverRequest->append((char)requestVal);
    serverRequest->append("\r\n\r\n");
    _socket->write(serverRequest);

}

QMap<int, CCPatientRecord*> CCPatientRecordControl::getPatientRecordsMap(){

    if (_patientRecordsMap.size() < 1)
    {
        refreshPatientRecords();
    }
    return _patientRecordsMap;
}

QVector<int> CCPatientRecordControl::getPatientsFiltered(bool useStatus, bool usePhysician, bool currentStatusOnly, CCConsultationRecordStatus status, bool consultingPhysicianOnly, QVector<int> physicianIds)
{
    // Update record on server
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }

    _socket->connectToServer();
    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 89;
    serverRequest->append((char)requestVal);

    if (useStatus)
    {
        serverRequest->append(CCUtils::getStatusStringForStatus(status).toLower()).append(",");
        serverRequest->append((currentStatusOnly ? "1," : "0,"));
    }
    else
    {
        serverRequest->append("-1,0,");
    }

    if (usePhysician && physicianIds.count() > 0)
    {
        serverRequest->append((consultingPhysicianOnly ? "1," : "0,"));
        for (int i = 0; i < physicianIds.count(); i++)
        {
            serverRequest->append(QString::number(physicianIds.at(i))).append(",");
        }
    }
    else
    {
        serverRequest->append("0,-1,");
    }
    serverRequest->append("\r\n\r\n");

    _socket->write(serverRequest);

    return _filteredPatients;
}
