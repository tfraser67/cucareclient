#include "systemhomewindow.h"
#include "ui_systemhomewindow.h"

SystemHomeWindow::SystemHomeWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SystemHomeWindow)
{
    ui->setupUi(this);
}

SystemHomeWindow::~SystemHomeWindow()
{
    delete ui;
}

void SystemHomeWindow::on_logoutButton_clicked()
{
    LoginWindow* l = new LoginWindow;
    l->ShowLoginScreen();
    this->close();
}

void SystemHomeWindow::on_auditPropertiesButton_clicked()
{
    SetAuditPropertiesWindow* sap = new SetAuditPropertiesWindow();
    sap->show();
}
