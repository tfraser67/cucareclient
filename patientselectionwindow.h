#ifndef PATIENTSELECTIONWINDOW_H
#define PATIENTSELECTIONWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "ccpatientrecord.h"

class PatientSelectionWindowDelegate
{
public:
    virtual void windowReturnedPatientId(int patientId) = 0;
};

namespace Ui {
class PatientSelectionWindow;
}

class PatientSelectionWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PatientSelectionWindow(QWidget *parent = 0, PatientSelectionWindowDelegate* delegate = 0);
    ~PatientSelectionWindow();

private slots:
    void on_selectPatientButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::PatientSelectionWindow *ui;
    QVector<CCPatientRecord*> patientList;
    PatientSelectionWindowDelegate* _delegate;

    void populateTable();
};

#endif // PATIENTSELECTIONWINDOW_H
