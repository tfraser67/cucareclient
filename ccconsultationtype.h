#ifndef CCCONSULTATIONTYPE_H
#define CCCONSULTATIONTYPE_H

#include <QString>
#include <QStringList>

class CCConsultationType
{
public:
    CCConsultationType(int aId, QString aTypeName);
    CCConsultationType(QString constructorString);
    ~CCConsultationType();

    int id;
    QString typeName;
};

#endif // CCCONSULTATIONTYPE_H
