#include "managepatientswindow.h"
#include "ui_managepatientswindow.h"
#include "ccphysicianrecordcontrol.h"
#include "ccclinicrecordcontrol.h"
#include "ccutils.h"

ManagePatientsWindow::ManagePatientsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ManagePatientsWindow)
{
    physicianWindow = NULL;

    ui->setupUi(this);

    ui->patientTableWidget->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    ui->patientTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->patientTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->patientTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    refreshPatientTable();
}

ManagePatientsWindow::~ManagePatientsWindow()
{
    delete ui;
}

void ManagePatientsWindow::refreshPatientTable()
{
    // Get patient info, fill table, rinse and repeat yo
    CCPatientRecordControl* patientControl = CCPatientRecordControl::sharedPatientRecordControl();
    patientControl->refreshPatientRecords();

    QMap<int, CCPatientRecord*> patientMap = patientControl->getPatientRecordsMap();
    QMap<int, CCPatientRecord*>::iterator i;
    int rowCount = ui->patientTableWidget->rowCount();
    for (int i = 0; i < rowCount; i++)
    {
        ui->patientTableWidget->removeRow(0);
    }
    patientRecordList.clear();
    for (i = patientMap.begin(); i != patientMap.end(); ++i)
    {
        _addPatientToTable(i.value());
    }

    ui->patientTableWidget->resizeColumnsToContents();
}

bool ManagePatientsWindow::_addPatientToTable(CCPatientRecord* record)
{

    int rowIndex = ui->patientTableWidget->rowCount();
    ui->patientTableWidget->insertRow(rowIndex);

    ui->patientTableWidget->setItem(rowIndex, 0, new QTableWidgetItem(record->lastName));
    ui->patientTableWidget->setItem(rowIndex, 1, new QTableWidgetItem(record->firstName));

    CCPhysicianRecord* physician = CCPhysicianRecordControl::sharedPhysicianRecordControl()->getPhysicianRecordForId(record->physicianId);
    QString fullPhysicianName = physician->firstName;
    fullPhysicianName = fullPhysicianName.append(" ").append(physician->lastName);
    ui->patientTableWidget->setItem(rowIndex, 2, new QTableWidgetItem(fullPhysicianName));

    ui->patientTableWidget->setItem(rowIndex, 3, new QTableWidgetItem(CCClinicRecordControl::sharedClinicRecordControl()->getClinicNameForId(physician->clinicId)));

    ui->patientTableWidget->setItem(rowIndex, 4, new QTableWidgetItem(CCUtils::getStatusStringForStatus(record->patientStatus)));

    ui->patientTableWidget->setVerticalHeaderItem(rowIndex, new QTableWidgetItem(""));

    patientRecordList.append(record);

    return true;
}

void ManagePatientsWindow::on_filterPhysiciansButton_clicked()
{
    // Pop up physician selector view
    if (physicianWindow == NULL)
    {
        physicianWindow = new PhysicianSelectionWindow(this, this, true);
        physicianWindow->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(physicianWindow, SIGNAL(destroyed()), this, SLOT(clearPhysicianWindow()));
    }

    if (physicianWindow)
    {
        if (physicianIdsFilter.count() > 0)
        {
            physicianWindow->preselectPhysicians(physicianIdsFilter);
        }

        Qt::WindowFlags eFlags = physicianWindow->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        physicianWindow->setWindowFlags(eFlags);
        physicianWindow->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        physicianWindow->setWindowFlags(eFlags);
        physicianWindow->show();
    }
}

void ManagePatientsWindow::windowReturnedPhysicianList(QVector<int> physicianArray)
{
    physicianIdsFilter = physicianArray;
}

void ManagePatientsWindow::on_applyFiltersButton_clicked()
{
    CCPatientRecordControl* patientControl = CCPatientRecordControl::sharedPatientRecordControl();

    // Get filtered results
    filteredPatients.clear();
    filteredPatients = patientControl->getPatientsFiltered((ui->statusComboBox->currentIndex() < 3), (physicianIdsFilter.count() > 0), ui->currentStatusCheck->isChecked(),
                                        (CCConsultationRecordStatus)ui->statusComboBox->currentIndex(), ui->primaryPhysicianCheck->isChecked(), physicianIdsFilter);

    // Applies filtering to table
    int rowCount = ui->patientTableWidget->rowCount();
    for (int i = 0; i < rowCount; i++)
    {
        ui->patientTableWidget->removeRow(0);
    }
    patientRecordList.clear();

    for (int i = 0; i < filteredPatients.count(); i++)
    {
        CCPatientRecord* record = patientControl->getPatientRecordForId(filteredPatients.at(i));
        if (record)
        {
            _addPatientToTable(record);
        }
    }
}

void ManagePatientsWindow::on_clearFiltersButton_clicked()
{
    // Clears out filters applied to table (essentially just reloads table)
    filteredPatients.clear();
    physicianIdsFilter.clear();
    ui->statusComboBox->setCurrentIndex(3);
    refreshPatientTable();
}

void ManagePatientsWindow::clearPhysicianWindow()
{
    physicianWindow = NULL;
}
