#ifndef CCDATACONTROL_H
#define CCDATACONTROL_H

#include "clientsocket.h"
#include <QObject>

class CCDataControl : public QObject
{
    Q_OBJECT
public:
    explicit CCDataControl(QObject *parent = 0);
    virtual void receiveServerData(QByteArray *received) = 0;

signals:


protected:
    clientSocket* _socket;
    bool _awaitingServerResponse;
};

#endif // CCDATACONTROL_H
