#ifndef CCAUDITCONTROL_H
#define CCAUDITCONTROL_H

#include "ccdatacontrol.h"
#include <QMap>
#include <QString>
#include <QTime>

class ccAuditControl: public CCDataControl
{
public:
    ccAuditControl();
    ~ccAuditControl();
    static ccAuditControl* sharedAuditControl();

    void refreshAuditControl();
    void saveAuditTime(QTime t);

    QTime getAuditTime();

    virtual void receiveServerData(QByteArray *received);

private:
    QTime time;
};

#endif // CCAUDITCONTROL_H

