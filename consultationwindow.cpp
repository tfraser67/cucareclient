#include "consultationwindow.h"
#include "ui_consultationwindow.h"
#include "manageconsultationswindow.h"
#include "patientselectionwindow.h"
#include "physicianselectionwindow.h"
#include "ccphysicianrecordcontrol.h"
#include "ccpatientrecordcontrol.h"
#include "ccclinicrecordcontrol.h"
#include "ccconsultationtypecontrol.h"
#include "ccconsultationrecordcontrol.h"

#include <QMap>
#include <QMessageBox>

ConsultationWindow::ConsultationWindow(CCConsultationWindowType windowType, CCConsultationRecord* record, bool isFollowUp, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ConsultationWindow)
{
    _patientWindow = NULL;
    _physicianWindow = NULL;

    _patientRecord = NULL;
    _physicianRecord = NULL;
    _windowType = windowType;
    _consultationRecord = record;
    _isFollowUp = isFollowUp;

    ui->setupUi(this);

    if (_windowType != CCConsultationWindowTypeView)
    {
        populateClinics();
        populateConsultationTypes();
    }

    if (_windowType == CCConsultationWindowTypeAdd)
    {
        ui->dateOfConsultDateTime->setTime(QTime::currentTime());
    }
    else
    {
        setupViewWithRecord();
        if (_windowType == CCConsultationWindowTypeView)
        {
            ui->statusComboBox->setEnabled(false);
            ui->clinicComboBox->setEnabled(false);
            ui->consultationTypeComboBox->setEnabled(false);
            ui->dateOfConsultDateTime->setEnabled(false);
        }
        ui->patientSelectButton->setEnabled(false);
        ui->physicianSelectButton->setEnabled(false);
    }
}

ConsultationWindow::~ConsultationWindow()
{
    delete ui;
}

void ConsultationWindow::closeEvent(QCloseEvent *)
{
    const QObjectList list = this->children();
    for (int i = 0; i < list.count(); i++)
    {
        QWidget* item = qobject_cast<QWidget*>(list.at(i));
        if (item != NULL)
        {
            item->close();
        }
    }
}

void ConsultationWindow::setupViewWithRecord()
{
    QString startBitString = "";

    startBitString = "Patient: ";
    CCPatientRecord* patient = CCPatientRecordControl::sharedPatientRecordControl()->getPatientRecordForId(_consultationRecord->patientId);
    ui->patientLabel->setText(startBitString.append(patient->firstName).append(" ").append(patient->lastName));

    startBitString = "Consulting Physician: ";
    CCPhysicianRecord* physician = CCPhysicianRecordControl::sharedPhysicianRecordControl()->getPhysicianRecordForId(_consultationRecord->physicianId);
    ui->physicianLabel->setText(startBitString.append(physician->firstName).append(" ").append(physician->lastName));


    if (_windowType == CCConsultationWindowTypeView)
    {
        startBitString = "Clinic: ";
        ui->clinicComboBox->addItem(CCClinicRecordControl::sharedClinicRecordControl()->getClinicNameForId(_consultationRecord->clinicId));

        startBitString = "Consultation Type: ";
        ui->consultationTypeComboBox->addItem(CCConsultationTypeControl::sharedConsultationTypeControl()->getConsultationTypeNameForId(_consultationRecord->consultationTypeId));

        ui->createConsultationButton->hide();
        ui->cancelButton->setText("Ok");
    }
    else if(_windowType == CCConsultationWindowTypeEdit){
        for (int i = 0; i < _clinicsList.count(); i++){
            if(_clinicsList.at(i)->clinicId == _consultationRecord->clinicId){
                ui->clinicComboBox->setCurrentIndex(i);
            }
        }
        for (int i = 0; i < _consultationsTypeList.count(); i++){
            if(_consultationsTypeList.at(i)->id == _consultationRecord->consultationTypeId){
                ui->consultationTypeComboBox->setCurrentIndex(i);
            }
        }

        ui->createConsultationButton->setText("Save");
        ui->createConsultationButton->show();
        ui->cancelButton->setText("Cancel");
    }
    else if(_windowType == CCConsultationWindowTypeAdd){
        ui->createConsultationButton->setText("Add");
        ui->createConsultationButton->show();
        ui->cancelButton->setText("Cancel");
    }



    ui->statusComboBox->setCurrentIndex((int)_consultationRecord->status);

    ui->additionalInformationTextEdit->setText(_consultationRecord->additionalInformation);
    ui->physicianNotesTextEdit->setText(_consultationRecord->physicianNotes);
    ui->diagnosisTextEdit->setText(_consultationRecord->diagnosis);
    ui->reasonForConsultationTextEdit->setText(_consultationRecord->reasonForConsultation);

    ui->dateOfConsultDateTime->setDateTime(_consultationRecord->dateOfConsult);
}

void ConsultationWindow::populateClinics()
{
    CCClinicRecordControl* clinRecControl = CCClinicRecordControl::sharedClinicRecordControl();

    ui->clinicComboBox->clear();
    _clinicsList.clear();

    QMap<int, CCClinicRecord*> clinRecMap = clinRecControl->getClinicRecordsMap();
    _clinicsList.reserve(clinRecMap.size());
    QMap<int, CCClinicRecord*>::iterator i;
    for (i = clinRecMap.begin(); i != clinRecMap.end(); ++i)
    {
        CCClinicRecord* record = (CCClinicRecord*)i.value();
        ui->clinicComboBox->addItem(record->clinicName);
        _clinicsList.append(record);
    }
}

void ConsultationWindow::populateConsultationTypes()
{
    CCConsultationTypeControl* typeControl = CCConsultationTypeControl::sharedConsultationTypeControl();

    ui->consultationTypeComboBox->clear();

    QMap<int, CCConsultationType*> typeMap = typeControl->getConsultationTypesMap();
    _consultationsTypeList.reserve(typeMap.size());
    QMap<int, CCConsultationType*>::iterator i;
    for (i = typeMap.begin(); i != typeMap.end(); ++i)
    {
        CCConsultationType* type = (CCConsultationType*)i.value();
        ui->consultationTypeComboBox->addItem(type->typeName);
        _consultationsTypeList.append(type);
    }
}

void ConsultationWindow::on_patientSelectButton_clicked()
{
    // Pop up patient selector view
    if (_patientWindow == NULL)
    {
        _patientWindow = new PatientSelectionWindow(this, this);
        _patientWindow->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(_patientWindow, SIGNAL(destroyed()), this, SLOT(clearPatientWindow()));
    }

    if (_patientWindow)
    {
        Qt::WindowFlags eFlags = _patientWindow->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        _patientWindow->setWindowFlags(eFlags);
        _patientWindow->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        _patientWindow->setWindowFlags(eFlags);
        _patientWindow->show();
    }
}

void ConsultationWindow::clearPatientWindow()
{
    _patientWindow = NULL;
}

void ConsultationWindow::on_physicianSelectButton_clicked()
{
    // Pop up physician selector view
    if (_physicianWindow == NULL)
    {
        _physicianWindow = new PhysicianSelectionWindow(this, this);
        _physicianWindow->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(_physicianWindow, SIGNAL(destroyed()), this, SLOT(clearPhysicianWindow()));
    }

    if (_physicianWindow)
    {
        Qt::WindowFlags eFlags = _physicianWindow->windowFlags();
        //eFlags |= Qt::WindowStaysOnTopHint;
        _physicianWindow->setWindowFlags(eFlags);
        _physicianWindow->show();
        //eFlags &= ~Qt::WindowStaysOnTopHint;
        _physicianWindow->setWindowFlags(eFlags);
        _physicianWindow->show();
    }
}

void ConsultationWindow::clearPhysicianWindow()
{
    _physicianWindow = NULL;
}

void ConsultationWindow::on_createConsultationButton_clicked()
{
    if (_windowType == CCConsultationWindowTypeAdd)
    {
        if (_patientRecord && _physicianRecord &&
                ui->clinicComboBox->currentIndex() < _clinicsList.count() &&
                ui->consultationTypeComboBox->currentIndex() < _consultationsTypeList.count() &&
                ui->dateOfConsultDateTime->dateTime() > QDateTime::currentDateTime())
        {
            int patientId = _patientRecord->ohipNumber;
            int physicianId = _physicianRecord->physicianId;
            int clinicId = ((CCClinicRecord*)_clinicsList.at(ui->clinicComboBox->currentIndex()))->clinicId;
            int consultationTypeId = ((CCConsultationType*)_consultationsTypeList.at(ui->consultationTypeComboBox->currentIndex()))->id;

            // Save record to database
            CCConsultationRecordControl* recordControl = CCConsultationRecordControl::sharedConsultationRecordControl();

            CCConsultationRecord* record = new CCConsultationRecord(patientId, physicianId, CCConsultationRecordStatusPending, clinicId,
                                                                    ui->dateOfConsultDateTime->dateTime(), consultationTypeId,
                                                                    ui->reasonForConsultationTextEdit->toPlainText(),
                                                                    ui->additionalInformationTextEdit->toPlainText(),
                                                                    ui->physicianNotesTextEdit->toPlainText(),
                                                                    ui->diagnosisTextEdit->toPlainText());

            recordControl->saveConsultationRecord(record);

            // Get parent to refresh
            ManageConsultationsWindow* mcw = qobject_cast<ManageConsultationsWindow*>(this->parent());
            if (mcw != NULL)
            {
                mcw->refreshConsultationsTable();
            }

            // Close window
            this->close();
        }
        else
        {
            // Error
            QMessageBox* alert = new QMessageBox(QMessageBox::Information, "Mandatory information incomplete", "You must select a patient, physician, clinic, consultation type and date in the future for the consultation in order to add this consultation.", QMessageBox::Ok, this);
            //alert->setWindowFlags(Qt::WindowStaysOnTopHint);
            alert->show();
        }
    }
    else if (_windowType == CCConsultationWindowTypeEdit)
    {
        if (ui->dateOfConsultDateTime->dateTime() > QDateTime::currentDateTime() || _consultationRecord->status > 0)
        {
            // Save record to database
            _consultationRecord->clinicId = _clinicsList.at(ui->clinicComboBox->currentIndex())->clinicId;
            _consultationRecord->dateOfConsult = ui->dateOfConsultDateTime->dateTime();
            _consultationRecord->status = (CCConsultationRecordStatus)ui->statusComboBox->currentIndex();
            _consultationRecord->additionalInformation = ui->additionalInformationTextEdit->toPlainText();
            _consultationRecord->physicianNotes = ui->physicianNotesTextEdit->toPlainText();
            _consultationRecord->diagnosis = ui->diagnosisTextEdit->toPlainText();
            _consultationRecord->reasonForConsultation = ui->reasonForConsultationTextEdit->toPlainText();

            CCConsultationRecordControl::sharedConsultationRecordControl()->saveConsultationRecord(_consultationRecord);

            // Get parent to refresh
            ManageConsultationsWindow* mcw = qobject_cast<ManageConsultationsWindow*>(this->parent());
            if (mcw != NULL)
            {
                mcw->refreshConsultationsTable();
            }
            this->close();
        }
        else
        {
            // Error message
            QMessageBox* alert = new QMessageBox(QMessageBox::Information, "Date is before current date", "You must select a date in the future in order to save edit.", QMessageBox::Ok, this);
            //alert->setWindowFlags(Qt::WindowStaysOnTopHint);
            alert->show();
        }
    }
}

void ConsultationWindow::updatePatient(CCPatientRecord* patient)
{
    _patientRecord = patient;

    QString patientText = "Patient: ";
    ui->patientLabel->setText(patientText.append(_patientRecord->firstName).append(" ").append(_patientRecord->lastName));
}

void ConsultationWindow::updatePhysician(CCPhysicianRecord* physician)
{
    _physicianRecord = physician;

    QString physicianText = "Physician: ";
    ui->physicianLabel->setText(physicianText.append(_physicianRecord->firstName).append(" ").append(_physicianRecord->lastName));
}

void ConsultationWindow::on_cancelButton_clicked()
{
    this->close();
}

void ConsultationWindow::windowReturnedPhysicianList(QVector<int> physicianArray)
{
    // Only uses single select, therego, only need 1 physician
    if (physicianArray.size() > 0)
    {
        qDebug() << "Got in";
        updatePhysician(CCPhysicianRecordControl::sharedPhysicianRecordControl()->getPhysicianRecordForId(physicianArray.at(0)));
    }
}

void ConsultationWindow::windowReturnedPatientId(int patientId)
{
    updatePatient(CCPatientRecordControl::sharedPatientRecordControl()->getPatientRecordForId(patientId));
}
