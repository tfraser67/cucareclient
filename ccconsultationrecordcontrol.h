#ifndef CCCONSULTATIONRECORDCONTROL_H
#define CCCONSULTATIONRECORDCONTROL_H

#include "ccconsultationrecord.h"
#include "ccdatacontrol.h"
#include <QMap>

class CCConsultationRecordControl : public CCDataControl
{
public:
    CCConsultationRecordControl();
    ~CCConsultationRecordControl();
    static CCConsultationRecordControl* sharedConsultationRecordControl();

    void refreshConsultationRecords();
    CCConsultationRecord* getConsultationRecordForId(int aConsultationRecordId);
    int getMapSize();
    QMap<int, CCConsultationRecord*> getConsultationRecordMap();

    bool saveConsultationRecord(CCConsultationRecord* record);

    virtual void receiveServerData(QByteArray *received);


private:
    QMap<int, CCConsultationRecord*> _consultationRecordsMap;

};

#endif // CCCONSULTATIONRECORDCONTROL_H
