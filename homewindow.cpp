#include "homewindow.h"
#include "ui_homewindow.h"
#include "manageconsultationswindow.h"

HomeWindow::HomeWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HomeWindow)
{
    mcw = NULL;
    mpw = NULL;

    ui->setupUi(this);
}

HomeWindow::~HomeWindow()
{
    delete ui;
}

void HomeWindow::closeEvent(QCloseEvent *)
{   
    const QObjectList list = this->children();
    for (int i = 0; i < list.count(); i++)
    {
        QWidget* item = qobject_cast<QWidget*>(list.at(i));
        if (item != NULL)
        {
            item->close();
        }
    }
}

void HomeWindow::on_consultButton_clicked()
{
    if (mcw == NULL)
    {
        mcw = new ManageConsultationsWindow(this);
        mcw->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(mcw, SIGNAL(destroyed()), this, SLOT(clearManageConsultationsWindow()));
    }

    if (mcw)
    {
        Qt::WindowFlags eFlags = mcw->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        mcw->setWindowFlags(eFlags);
        mcw->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        mcw->setWindowFlags(eFlags);
        mcw->show();
    }
}

void HomeWindow::clearManageConsultationsWindow()
{
    mcw = NULL;
}

void HomeWindow::clearManagePatientsWindow()
{
    mpw = NULL;
}

void HomeWindow::on_logoutButton_clicked()
{
    LoginWindow* l = new LoginWindow;
    l->ShowLoginScreen();
    this->close();
}

void HomeWindow::on_patientButton_clicked()
{
    if (mpw == NULL)
    {
        mpw = new ManagePatientsWindow(this);
        mpw->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(mpw, SIGNAL(destroyed()), this, SLOT(clearManagePatientsWindow()));
    }

    if (mpw)
    {
        Qt::WindowFlags eFlags = mpw->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        mpw->setWindowFlags(eFlags);
        mpw->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        mpw->setWindowFlags(eFlags);
        mpw->show();
    }
}
