#include "clientsocket.h"
#include "ccdatacontrol.h"
#include "ccuserdatacontrol.h"

clientSocket::clientSocket(QObject *parent) :
    QObject(parent),
    socket(NULL)
{


}

clientSocket::~clientSocket()
{
    if (socket != NULL)
    {
        socket->close();
    }
}

void clientSocket::connectToServer()
{
    socket = new QTcpSocket(this);

    connect(socket,SIGNAL(connected()),this,SLOT(connected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(socket,SIGNAL(bytesWritten(qint64)),this,SLOT(bytesWritten(qint64)));

  //  qDebug() << "Connecting...";

    //qDebug() << CCUserDataControl::sharedUserDataControl()->getIpAddressOfServer();
    socket->connectToHost(CCUserDataControl::sharedUserDataControl()->getIpAddressOfServer(),60000);

    if(!socket->waitForConnected(1000))
    {
        qDebug() << "Error:" << socket->errorString();
    }
}

void clientSocket::disconnectFromServer()
{
    socket->disconnectFromHost();
}

void clientSocket::connected()
{
 //   qDebug() << "Connected!";
}
void clientSocket::disconnected()
{
  //  qDebug() << "Disconnected!";
}

void clientSocket::bytesWritten(qint64 bytes)
{
    //qDebug() << "we wrote" << bytes;
}

void clientSocket::readyRead()
{
    CCDataControl* parentControl = qobject_cast<CCDataControl*>(this->parent());
    while(socket->bytesAvailable())
    {
        QByteArray fromserver = socket->readLine();
        if (parentControl != NULL)
        {
            parentControl->receiveServerData(&fromserver);
        }
    }
}

void clientSocket::write(QByteArray *toSend)
{
    socket->write(*toSend);
    if (!socket->waitForBytesWritten(1000))
    {

    }
    if (!socket->waitForReadyRead(1000))
    {

    }
    if (!socket->waitForDisconnected(10000))
    {

    }
}
