#include "ccconsultationrecord.h"

#include <QDebug>

CCConsultationRecord::CCConsultationRecord(int aPatientId, int aPhysicianId, CCConsultationRecordStatus aStatus,
                                           int aClinicId, QDateTime aDate, int aConsultationTypeId,
                                           QString aReasonForConsult, QString aAdditionalInfo,
                                           QString aPhysicianNotes, QString aDiagnosis) :
    patientId(aPatientId),
    physicianId(aPhysicianId),
    status(aStatus),
    clinicId(aClinicId),
    dateOfConsult(aDate),
    consultationTypeId(aConsultationTypeId),
    reasonForConsultation(aReasonForConsult),
    additionalInformation(aAdditionalInfo),
    physicianNotes(aPhysicianNotes),
    diagnosis(aDiagnosis),
    refNum(-1)
{
}

CCConsultationRecord::CCConsultationRecord(QString constructorString)
{
    QStringList parseString;
    parseString = constructorString.split(",");

    refNum = parseString.at(0).trimmed().toInt();
    QString theStatus = parseString.at(1).trimmed();
    if (theStatus == "pending")
    {
        status = CCConsultationRecordStatusPending;
    }
    else if (theStatus == "overdue")
    {
        status = CCConsultationRecordStatusOverdue;
    }
    else
    {
        status = CCConsultationRecordStatusComplete;
    }

    consultationTypeId = parseString.at(2).trimmed().toInt();
    physicianId = parseString.at(3).trimmed().toInt();
    patientId = parseString.at(4).trimmed().toInt();
    clinicId = parseString.at(5).trimmed().toInt();

    QString stringDateTime = parseString.at(6).trimmed().append(" ");
    stringDateTime.append(parseString.at(7)).trimmed();
    dateOfConsult = QDateTime::fromString(stringDateTime,"yyyy-MM-dd H:mm:ss");

    reasonForConsultation = parseString.at(8).trimmed();
    physicianNotes = parseString.at(9).trimmed();
    additionalInformation = parseString.at(10).trimmed();
    diagnosis = parseString.at(11).trimmed();
}

CCConsultationRecord::~CCConsultationRecord()
{

}
