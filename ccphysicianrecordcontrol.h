#ifndef CCPHYSICIANRECORDCONTROL_H
#define CCPHYSICIANRECORDCONTROL_H

#include <QMap>
#include "ccdatacontrol.h"
#include "ccphysicianrecord.h"
#include "ccconsultationrecord.h"

class CCPhysicianRecordControl : public CCDataControl
{
public:
    CCPhysicianRecordControl();
    ~CCPhysicianRecordControl();
    static CCPhysicianRecordControl* sharedPhysicianRecordControl();

    void refreshPhysicianRecords();
    CCPhysicianRecord* getPhysicianRecordForId(int aPhysicianRecordId);
    QMap<int, CCPhysicianRecord*> getPhysicianRecordsMap();

    QVector<int> getPhysiciansFiltered(QString queriedFirstName, QString queriedLastName, QString queriedClinicName);

    virtual void receiveServerData(QByteArray *received);

private:
    QMap<int, CCPhysicianRecord*> _physicianRecordsMap;
    QVector<int> _filteredPhysicians;

    bool applyingFilter;
};

#endif // CCPHYSICIANRECORDCONTROL_H
