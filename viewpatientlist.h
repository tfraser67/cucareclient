#ifndef VIEWPATIENTLIST_H
#define VIEWPATIENTLIST_H

#include <QMainWindow>

namespace Ui {
class ViewPatientList;
}

class ViewPatientList : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ViewPatientList(QWidget *parent = 0);
    ~ViewPatientList();
    
private:
    Ui::ViewPatientList *ui;
};

#endif // VIEWPATIENTLIST_H
