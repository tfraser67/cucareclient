#include "ccclinicrecordcontrol.h"

CCClinicRecordControl* CCClinicRecordControlSingleton = 0;

CCClinicRecordControl::CCClinicRecordControl()
{
}

CCClinicRecordControl::~CCClinicRecordControl()
{
}

CCClinicRecordControl* CCClinicRecordControl::sharedClinicRecordControl()
{
    if (CCClinicRecordControlSingleton == 0)
    {
        CCClinicRecordControlSingleton = new CCClinicRecordControl();
    }
    return CCClinicRecordControlSingleton;
}

void CCClinicRecordControl::receiveServerData(QByteArray *received)
{
    QString constructorString = QString(*received);

    qDebug() << constructorString;

    if (constructorString.contains("SERVER_CONNECTION_CONFIRM"))
    {
        // Server connection completed
        return;
    }
    else if (constructorString.contains("MESSAGE_DUMP_END"))
    {
        // Clinic list completely returned
        _awaitingServerResponse = false;
        _socket->disconnectFromServer();
    }
    else
    {
        CCClinicRecord* newRecord = new CCClinicRecord(constructorString);
        _clinicRecordsMap.insert(newRecord->clinicId, newRecord);
    }
}

QString CCClinicRecordControl::getClinicNameForId(int aClinicId)
{
    if (_clinicRecordsMap.size() < 1 || (!_clinicRecordsMap.contains(aClinicId)))
    {
        this->refreshClinicRecords();
    }

    if (_clinicRecordsMap.contains(aClinicId))
    {
        return _clinicRecordsMap[aClinicId]->clinicName;
    }

    return "Unknown Clinic";
}

void CCClinicRecordControl::refreshClinicRecords()
{
    _clinicRecordsMap.clear();

    // call server and refresh clinics map
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }

    _socket->connectToServer();
    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 75;
    serverRequest->append((char)requestVal);
    serverRequest->append("\r\n\r\n");
    _socket->write(serverRequest);
}

QMap<int, CCClinicRecord*> CCClinicRecordControl::getClinicRecordsMap()
{
    if (_clinicRecordsMap.size() < 1)
    {
        refreshClinicRecords();
    }

    return _clinicRecordsMap;
}
