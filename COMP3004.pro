#-------------------------------------------------
#
# Project created by QtCreator 2012-10-24T18:21:28
#
#-------------------------------------------------

QT       += core gui
QT      += network

TARGET = COMP3004
TEMPLATE = app


SOURCES += main.cpp\
        homewindow.cpp \
    loginwindow.cpp \
    systemhomewindow.cpp \
    manageconsultationswindow.cpp \
    ccuserdatacontrol.cpp \
    ccconsultationrecord.cpp \
    ccpatientrecord.cpp \
    ccphysicianrecord.cpp \
    ccclinicrecord.cpp \
    ccconsultationtype.cpp \
    ccconsultationtypecontrol.cpp \
    ccutils.cpp \
    ccpatientrecordcontrol.cpp \
    ccclinicrecordcontrol.cpp \
    ccphysicianrecordcontrol.cpp \
    ccconsultationrecordcontrol.cpp \
    patientselectionwindow.cpp \
    physicianselectionwindow.cpp \
    clientsocket.cpp \
    ccdatacontrol.cpp \
    viewpatientlist.cpp \
    consultationwindow.cpp \
    managepatientswindow.cpp \
    setauditpropertieswindow.cpp \
    ccauditcontrol.cpp


HEADERS  += homewindow.h \
    loginwindow.h \
    systemhomewindow.h \
    manageconsultationswindow.h \
    ccuserdatacontrol.h \
    ccconsultationrecord.h \
    ccpatientrecord.h \
    ccphysicianrecord.h \
    ccclinicrecord.h \
    ccconsultationtype.h \
    ccconsultationtypecontrol.h \
    ccutils.h \
    ccpatientrecordcontrol.h \
    ccclinicrecordcontrol.h \
    ccphysicianrecordcontrol.h \
    ccconsultationrecordcontrol.h \
    patientselectionwindow.h \
    physicianselectionwindow.h \
    clientsocket.h \
    ccdatacontrol.h \
    viewpatientlist.h \
    consultationwindow.h \
    managepatientswindow.h \
    setauditpropertieswindow.h \
    ccauditcontrol.h

FORMS    += homewindow.ui \
    loginwindow.ui \
    systemhomewindow.ui \
    manageconsultationswindow.ui \
    patientselectionwindow.ui \
    physicianselectionwindow.ui \
    viewpatientlist.ui \
    consultationwindow.ui \
    managepatientswindow.ui \
    setauditpropertieswindow.ui

OTHER_FILES += \
    images/overdue.png \
    images/stethoscope.png \
    images/patient_treatment.png \
    images/patient_chart.png \
    images/medical_record.png \
    images/ecg_chart.png \
    images/doctor.png

RESOURCES += \
    imageResources.qrc
