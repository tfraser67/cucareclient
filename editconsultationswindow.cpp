#include "editconsultationswindow.h"
#include "ui_editconsultationswindow.h"
#include "ccconsultationtypecontrol.h"
#include "manageconsultationswindow.h"
#include "ccpatientrecordcontrol.h"
#include "ccphysicianrecordcontrol.h"
#include "ccconsultationrecordcontrol.h"

#include <QMessageBox>

EditConsultationsWindow::EditConsultationsWindow(CCConsultationRecord *aRecord, QWidget *parent) :
    QMainWindow(parent),
    consultationRecord(aRecord),
    ui(new Ui::EditConsultationsWindow)
{
    ui->setupUi(this);

    this->_setupViewWithRecord();
}

EditConsultationsWindow::~EditConsultationsWindow()
{
    delete ui;
}

void EditConsultationsWindow::closeEvent(QCloseEvent *)
{
    const QObjectList list = this->children();
    for (int i = 0; i < list.count(); i++)
    {
        QWidget* item = qobject_cast<QWidget*>(list.at(i));
        if (item != NULL)
        {
            item->close();
        }
    }
}

void EditConsultationsWindow::_setupViewWithRecord()
{
    QString startBitString = "";

    startBitString = "Patient: ";
    CCPatientRecord* patient = CCPatientRecordControl::sharedPatientRecordControl()->getPatientRecordForId(consultationRecord->patientId);
    ui->patientLabel->setText(startBitString.append(patient->firstName).append(" ").append(patient->lastName));

    startBitString = "Consulting Physician: ";
    CCPhysicianRecord* physician = CCPhysicianRecordControl::sharedPhysicianRecordControl()->getPhysicianRecordForId(consultationRecord->physicianId);
    ui->physicianLabel->setText(startBitString.append(physician->firstName).append(" ").append(physician->lastName));

    startBitString = "Clinic: ";
    ui->clinicLabel->setText(startBitString.append(CCClinicRecordControl::sharedClinicRecordControl()->getClinicNameForId(consultationRecord->clinicId)));

    startBitString = "Consultation Type: ";
    ui->consultationTypeLabel->setText(startBitString.append(QString::number(consultationRecord->consultationTypeId)).append(CCConsultationTypeControl::sharedConsultationTypeControl()->getConsultationTypeNameForId(consultationRecord->consultationTypeId)));

    ui->statusComboBox->setCurrentIndex((int)consultationRecord->status);
    if (ui->statusComboBox->currentIndex() > 0)
    {
        ui->statusComboBox->setEnabled(false);
    }

    ui->additionalInformationTextEdit->setText(consultationRecord->additionalInformation);
    ui->physicianNotesTextEdit->setText(consultationRecord->physicianNotes);
    ui->diagnosisTextEdit->setText(consultationRecord->diagnosis);
    ui->reasonForConsultationTextEdit->setText(consultationRecord->reasonForConsultation);

    ui->dateOfConsultDateTime->setDateTime(consultationRecord->dateOfConsult);
}

void EditConsultationsWindow::on_saveChangesButton_clicked()
{
    if (ui->dateOfConsultDateTime->dateTime() > QDateTime::currentDateTime() || ui->statusComboBox->currentIndex() > 0)
    {
        // Save record to database
        consultationRecord->status = (CCConsultationRecordStatus)ui->statusComboBox->currentIndex();
        consultationRecord->additionalInformation = ui->additionalInformationTextEdit->toPlainText();
        consultationRecord->physicianNotes = ui->physicianNotesTextEdit->toPlainText();
        consultationRecord->diagnosis = ui->diagnosisTextEdit->toPlainText();
        consultationRecord->reasonForConsultation = ui->reasonForConsultationTextEdit->toPlainText();
        consultationRecord->dateOfConsult = ui->dateOfConsultDateTime->dateTime();
       // consultationRecord->consultationTypeId = (ui->consultationTypeLabel->text().remove("^[a-zA-Z_]*$"));
       // consultationRecord
       // consultationRecord
       // consultationRecord
      //  consultationRecord
      //  consultationRecord

        CCConsultationRecordControl::sharedConsultationRecordControl()->saveConsultationRecord(consultationRecord);

        // Get parent to refresh
        ManageConsultationsWindow* mcw = qobject_cast<ManageConsultationsWindow*>(this->parent());
        if (mcw != NULL)
        {
            mcw->refreshConsultationsTable();
        }

        // Close window
        this->close();
    }
    else
    {
        // Error message
        QMessageBox* alert = new QMessageBox(QMessageBox::Information, "Date is before current date", "You must select a valid date in the future in order to save changes.", QMessageBox::Ok, this);
        alert->setWindowFlags(Qt::WindowStaysOnTopHint);
        alert->show();
    }
}

void EditConsultationsWindow::on_cancelButton_clicked()
{
    this->close();
}
