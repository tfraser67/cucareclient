#include "manageconsultationswindow.h"
#include "ui_manageconsultationswindow.h"
#include "consultationwindow.h"
#include "ccpatientrecordcontrol.h"
#include "ccphysicianrecordcontrol.h"
#include "ccconsultationrecordcontrol.h"
#include "ccutils.h"

#include <QMessageBox>
#include <QModelIndexList>
#include <QObjectList>

ManageConsultationsWindow::ManageConsultationsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ManageConsultationsWindow)
{
    addWindow = NULL;
    editWindow = NULL;
    viewWindow = NULL;

    ui->setupUi(this);

    ui->consultationTableWidget->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    ui->consultationTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->consultationTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->consultationTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    refreshConsultationsTable();
}

ManageConsultationsWindow::~ManageConsultationsWindow()
{
    delete ui;
}

bool ManageConsultationsWindow::_addConsultationToTable(CCConsultationRecord* record)
{

    int rowIndex = ui->consultationTableWidget->rowCount();
    ui->consultationTableWidget->insertRow(rowIndex);

    QString statusString = CCUtils::getStatusStringForStatus(record->status);
    ui->consultationTableWidget->setItem(rowIndex, 0, new QTableWidgetItem(statusString));

    CCPatientRecord* patient = CCPatientRecordControl::sharedPatientRecordControl()->getPatientRecordForId(record->patientId);
    QString fullPatientName = patient->firstName;
    fullPatientName = fullPatientName.append(" ").append(patient->lastName);
    ui->consultationTableWidget->setItem(rowIndex, 1, new QTableWidgetItem(fullPatientName));

    CCPhysicianRecord* physician = CCPhysicianRecordControl::sharedPhysicianRecordControl()->getPhysicianRecordForId(record->physicianId);
    QString fullPhysicianName = physician->firstName;
    fullPhysicianName = fullPhysicianName.append(" ").append(physician->lastName);
    ui->consultationTableWidget->setItem(rowIndex, 2, new QTableWidgetItem(fullPhysicianName));

    ui->consultationTableWidget->setItem(rowIndex, 3, new QTableWidgetItem(CCClinicRecordControl::sharedClinicRecordControl()->getClinicNameForId(record->clinicId)));
    ui->consultationTableWidget->setItem(rowIndex, 4, new QTableWidgetItem(record->dateOfConsult.toString()));

    ui->consultationTableWidget->setVerticalHeaderItem(rowIndex, new QTableWidgetItem(""));

    consultationRecordList.append(record);

    return true;
}

void ManageConsultationsWindow::on_actionAdd_New_Consultation_triggered()
{
    // Open new add window
    if (addWindow == NULL)
    {
        addWindow = new ConsultationWindow(CCConsultationWindowTypeAdd, 0, false, this);
        addWindow->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(addWindow, SIGNAL(destroyed()), this, SLOT(clearAddWindow()));
    }

    if (addWindow)
    {
        Qt::WindowFlags eFlags = addWindow->windowFlags();
        //eFlags |= Qt::WindowStaysOnTopHint;
        addWindow->setWindowFlags(eFlags);
        addWindow->show();
        //eFlags &= ~Qt::WindowStaysOnTopHint;
        addWindow->setWindowFlags(eFlags);
        addWindow->show();
    }
}

void ManageConsultationsWindow::clearAddWindow()
{
    addWindow = NULL;
}

void ManageConsultationsWindow::on_actionEdit_Selected_Consultation_triggered()
{
    QModelIndexList selectedRows = ui->consultationTableWidget->selectionModel()->selectedRows();
    if (selectedRows.count() > 0)
    {
        int index = selectedRows.at(0).row();

        // Check that the index exists within the array and grab the consultations record

        if (index >= 0 && index < consultationRecordList.count())
        {
            CCConsultationRecord* record = consultationRecordList.at(index);

            if (editWindow == NULL)
            {
                editWindow = new ConsultationWindow(CCConsultationWindowTypeEdit, record, false, this);
                editWindow->setAttribute(Qt::WA_DeleteOnClose, true);
                QObject::connect(editWindow, SIGNAL(destroyed()), this, SLOT(clearEditWindow()));
            }

            if (editWindow)
            {
                Qt::WindowFlags eFlags = editWindow->windowFlags();
                eFlags |= Qt::WindowStaysOnTopHint;
                editWindow->setWindowFlags(eFlags);
                editWindow->show();
                eFlags &= ~Qt::WindowStaysOnTopHint;
                editWindow->setWindowFlags(eFlags);
                editWindow->show();
            }
        }

    }
    else
    {
        // Message with "selected a row to edit"
        QMessageBox* alert = new QMessageBox(QMessageBox::Information, "No consultation was selected", "You must select a consultation in order to edit.", QMessageBox::Ok, this);
        //alert->setWindowFlags(Qt::WindowStaysOnTopHint);
        alert->show();
    }
}

void ManageConsultationsWindow::clearEditWindow()
{
    editWindow = NULL;
}

void ManageConsultationsWindow::closeEvent(QCloseEvent *)
{
    const QObjectList list = this->children();
    for (int i = 0; i < list.count(); i++)
    {
        QWidget* item = qobject_cast<QWidget*>(list.at(i));
        if (item != NULL)
        {
            item->close();
        }
    }
}

void ManageConsultationsWindow::refreshConsultationsTable()
{
    int rowCount = ui->consultationTableWidget->rowCount();
    for (int i = 0; i < rowCount; i++)
    {
        ui->consultationTableWidget->removeRow(0);
    }
    consultationRecordList.clear();

    // Refresh yo!
    CCConsultationRecordControl* consultControl = CCConsultationRecordControl::sharedConsultationRecordControl();

    QMap<int, CCConsultationRecord*> consultMap = consultControl->getConsultationRecordMap();
    QMap<int, CCConsultationRecord*>::iterator i;
    for (i = consultMap.begin(); i != consultMap.end(); ++i)
    {
        _addConsultationToTable(i.value());
    }

    ui->consultationTableWidget->resizeColumnsToContents();
}

void ManageConsultationsWindow::on_actionRefresh_Consultations_triggered()
{
    CCConsultationRecordControl::sharedConsultationRecordControl()->refreshConsultationRecords();

    refreshConsultationsTable();
}

void ManageConsultationsWindow::on_actionView_Consultation_triggered()
{
    QModelIndexList selectedRows = ui->consultationTableWidget->selectionModel()->selectedRows();
    if (selectedRows.count() > 0)
    {
        int index = selectedRows.at(0).row();

        // Check that the index exists within the array and grab the consultations record

        if (index >= 0 && index < consultationRecordList.count())
        {
            CCConsultationRecord* record = consultationRecordList.at(index);

            if (viewWindow == NULL)
            {
                viewWindow = new ConsultationWindow(CCConsultationWindowTypeView, record, false, this);
                viewWindow->setAttribute(Qt::WA_DeleteOnClose, true);
                QObject::connect(viewWindow, SIGNAL(destroyed()), this, SLOT(clearViewWindow()));
            }

            if (viewWindow)
            {
                Qt::WindowFlags eFlags = viewWindow->windowFlags();
                eFlags |= Qt::WindowStaysOnTopHint;
                viewWindow->setWindowFlags(eFlags);
                viewWindow->show();
                eFlags &= ~Qt::WindowStaysOnTopHint;
                viewWindow->setWindowFlags(eFlags);
                viewWindow->show();
            }
        }

    }
    else
    {
        // Message with "selected a row to edit"
        QMessageBox* alert = new QMessageBox(QMessageBox::Information, "No consultation was selected", "You must select a consultation in order to edit.", QMessageBox::Ok, this);
        alert->setWindowFlags(Qt::WindowStaysOnTopHint);
        alert->show();
    }

}

void ManageConsultationsWindow::clearViewWindow()
{
    viewWindow = NULL;
}
