#include "physicianselectionwindow.h"
#include "ui_physicianselectionwindow.h"
#include "addconsultationwindow.h"
#include "ccphysicianrecordcontrol.h"
#include "ccclinicrecordcontrol.h"

#include <QMessageBox>

PhysicianSelectionWindow::PhysicianSelectionWindow(QWidget *parent, PhysicianSelectionWindowDelegate* delegate, bool multiSelect) :
    QMainWindow(parent),
    _delegate(delegate),
    ui(new Ui::PhysicianSelectionWindow)
{
    ui->setupUi(this);

    ui->physicianTableWidget->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    ui->physicianTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    if (!multiSelect){
        ui->physicianTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    }
    else{
        ui->physicianTableWidget->setSelectionMode(QAbstractItemView::MultiSelection);
    }
    ui->physicianTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    populateTable();
}

PhysicianSelectionWindow::~PhysicianSelectionWindow()
{
    delete ui;
}

void PhysicianSelectionWindow::preselectPhysicians(QVector<int> preselectedPhysicians)
{
    // Highlight all of the physicians for ids in vector
    for (int i = 0; i < preselectedPhysicians.count(); i++)
    {
        for (int k = 0; k < physicianList.count(); k++)
        {
            if (preselectedPhysicians.at(i) == physicianList.at(k)->physicianId)
            {
                ui->physicianTableWidget->selectRow(k);
            }
            continue;
        }
    }
}

void PhysicianSelectionWindow::populateTable()
{
    CCPhysicianRecordControl* physicianControl = CCPhysicianRecordControl::sharedPhysicianRecordControl();

    physicianControl->refreshPhysicianRecords();
    QMap<int, CCPhysicianRecord*> physicianMap = physicianControl->getPhysicianRecordsMap();
    QMap<int, CCPhysicianRecord*>::iterator i;

    int rowCount = ui->physicianTableWidget->rowCount();
    for (int i = 0; i < rowCount; i++)
    {
        ui->physicianTableWidget->removeRow(0);
    }

    physicianList.clear();
    for (i = physicianMap.begin(); i != physicianMap.end(); ++i)
    {
        _addPhysicianToTable(i.value());
    }
}

void PhysicianSelectionWindow::_addPhysicianToTable(CCPhysicianRecord* rec)
{
    int rowIndex = ui->physicianTableWidget->rowCount();
    ui->physicianTableWidget->insertRow(rowIndex);

    ui->physicianTableWidget->setItem(rowIndex, 0, new QTableWidgetItem(rec->lastName));
    ui->physicianTableWidget->setItem(rowIndex, 1, new QTableWidgetItem(rec->firstName));
    ui->physicianTableWidget->setItem(rowIndex, 2, new QTableWidgetItem(CCClinicRecordControl::sharedClinicRecordControl()->getClinicNameForId(rec->clinicId)));
    ui->physicianTableWidget->setVerticalHeaderItem(rowIndex, new QTableWidgetItem(""));
    physicianList.append(rec);
}

void PhysicianSelectionWindow::on_selectPatientButton_clicked()
{

    QModelIndexList selectedRows = ui->physicianTableWidget->selectionModel()->selectedRows();
    if (selectedRows.count() > 0)
    {
        // Get parent to refresh
        if (_delegate != NULL)
        {
            // Check that the index exists within the array and grab the consultations record

            QVector<int> physicianIds;
            for (int i = 0; i < selectedRows.count(); i++)
            {
                int index = selectedRows.at(i).row();
                if (index >= 0 && index < physicianList.count())
                {
                    physicianIds.append(physicianList.at(index)->physicianId);
                }

                // Update with selected physician
                _delegate->windowReturnedPhysicianList(physicianIds);

                // Close window
                this->close();
            }
        }
    }
    else
    {
        // Message with "selected a row"
        QMessageBox* alert = new QMessageBox(QMessageBox::Information, "No physician was selected", "You must select a physician.", QMessageBox::Ok, this);
        alert->setWindowFlags(Qt::WindowStaysOnTopHint);
        alert->show();

    }
}

void PhysicianSelectionWindow::on_cancelButton_clicked()
{
    this->close();
}


void PhysicianSelectionWindow::on_clearFiltersButton_clicked()
{
    populateTable();
}

void PhysicianSelectionWindow::on_applyFiltersButton_clicked()
{
    CCPhysicianRecordControl* physicianControl = CCPhysicianRecordControl::sharedPhysicianRecordControl();

    // Get filtered results
    filteredPhysicians.clear();
    filteredPhysicians = physicianControl->getPhysiciansFiltered(ui->physicianFirstNameLineEdit->text().trimmed(), ui->physicianLastNameLineEdit->text().trimmed(), ui->physicianClinicLineEdit->text().trimmed());

    // Applies filtering to table
    int rowCount = ui->physicianTableWidget->rowCount();
    for (int i = 0; i < rowCount; i++)
    {
        ui->physicianTableWidget->removeRow(0);
    }

    physicianList.clear();

    for (int i = 0; i < filteredPhysicians.count(); i++)
    {
        CCPhysicianRecord* record = physicianControl->getPhysicianRecordForId(filteredPhysicians.at(i));
        if (record)
        {
            _addPhysicianToTable(record);
        }
    }
}
