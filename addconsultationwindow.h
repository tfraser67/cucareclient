#ifndef ADDCONSULTATIONWINDOW_H
#define ADDCONSULTATIONWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "ccpatientrecord.h"
#include "ccphysicianrecord.h"
#include "patientselectionwindow.h"
#include "physicianselectionwindow.h"
#include "ccclinicrecordcontrol.h"
#include "ccconsultationtypecontrol.h"
#include "ccconsultationrecord.h"

namespace Ui {
class AddConsultationWindow;
}

class AddConsultationWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit AddConsultationWindow(QWidget *parent = 0);
    ~AddConsultationWindow();
    void closeEvent(QCloseEvent *);
    
    void updatePatient(CCPatientRecord* patient);
    void updatePhysician(CCPhysicianRecord* physician);

private slots:
    void on_patientSelectButton_clicked();
    void on_physicianSelectButton_clicked();
    void on_createConsultationButton_clicked();

    void clearPatientWindow();
    void clearPhysicianWindow();

    void on_cancelButton_clicked();

private:
    void populateClinics();
    void populateConsultationTypes();

    Ui::AddConsultationWindow *ui;
    CCPatientRecord* _patientRecord;
    CCPhysicianRecord* _physicianRecord;
    PatientSelectionWindow* patientWindow;
    PhysicianSelectionWindow* physicianWindow;
    QVector<CCClinicRecord*> clinicsList;
    QVector<CCConsultationType*> consultationsTypeList;
};

#endif // ADDCONSULTATIONWINDOW_H
