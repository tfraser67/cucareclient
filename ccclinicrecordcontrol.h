#ifndef CCCLINICRECORDCONTROL_H
#define CCCLINICRECORDCONTROL_H

#include "ccclinicrecord.h"
#include "ccdatacontrol.h"
#include <QMap>
#include <QString>

class CCClinicRecordControl : public CCDataControl
{
public:
    CCClinicRecordControl();
    ~CCClinicRecordControl();
    static CCClinicRecordControl* sharedClinicRecordControl();

    void refreshClinicRecords();
    QString getClinicNameForId(int aClinicId);

    QMap<int, CCClinicRecord*> getClinicRecordsMap();

    virtual void receiveServerData(QByteArray *received);

private:
    QMap<int, CCClinicRecord*> _clinicRecordsMap;
};

#endif // CCCLINICRECORDCONTROL_H
