#ifndef CLIENTSOCKET_H
#define CLIENTSOCKET_H

#include <QObject>
#include <QDebug>
#include <QTcpSocket>
#include <QAbstractSocket>

class clientSocket : public QObject
{
    Q_OBJECT
public:
    explicit clientSocket(QObject *parent = 0);
    ~clientSocket();
    void connectToServer();
    void disconnectFromServer();
    void write(QByteArray*);

public slots:
    void connected();
    void disconnected();
    void bytesWritten(qint64 bytes);
    void readyRead();

private:
    QTcpSocket *socket;

};
#endif // CLIENTSOCKET_H
