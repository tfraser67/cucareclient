#include "ccconsultationrecordcontrol.h"
#include "ccutils.h"
#include "clientsocket.h"
#include <QDebug>

CCConsultationRecordControl* CCConsultationRecordControlSingleton = 0;

CCConsultationRecordControl::CCConsultationRecordControl()
{
}

CCConsultationRecordControl::~CCConsultationRecordControl()
{

}

CCConsultationRecordControl* CCConsultationRecordControl::sharedConsultationRecordControl()
{
    if (CCConsultationRecordControlSingleton == 0)
    {
        CCConsultationRecordControlSingleton = new CCConsultationRecordControl();
    }

    return CCConsultationRecordControlSingleton;
}

void CCConsultationRecordControl::receiveServerData(QByteArray *received)
{
    QString constructorString = QString(*received);

    //qDebug() << constructorString;

    if (constructorString.contains("SERVER_CONNECTION_CONFIRM"))
    {
        // Server connection completed
        return;
    }
    else if (constructorString.contains("MESSAGE_DUMP_END"))
    {
        // Clinic list completely returned
        _awaitingServerResponse = false;
        _socket->disconnectFromServer();
    }
    else
    {
        CCConsultationRecord* newRecord = new CCConsultationRecord(constructorString);
        _consultationRecordsMap.insert(newRecord->refNum, newRecord);
    }
}

CCConsultationRecord* CCConsultationRecordControl::getConsultationRecordForId(int aConsultationRecordId)
{
    if (_consultationRecordsMap.size() < 1 || (!_consultationRecordsMap.contains(aConsultationRecordId)))
    {
        this->refreshConsultationRecords();
    }

    if (_consultationRecordsMap.contains(aConsultationRecordId))
    {
        return _consultationRecordsMap[aConsultationRecordId];
    }

    return NULL;
}

void CCConsultationRecordControl::refreshConsultationRecords()
{
    _consultationRecordsMap.clear();

    // call server and refresh map
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }

    _socket->connectToServer();
    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 74;
    serverRequest->append((char)requestVal);
    serverRequest->append("\r\n\r\n");
    _socket->write(serverRequest);
}

bool CCConsultationRecordControl::saveConsultationRecord(CCConsultationRecord* record)
{
    // Update record on server
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }
    _socket->connectToServer();


    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 71;
    serverRequest->append((char)requestVal);

    serverRequest->append(QString::number(record->refNum)).append(",");
    serverRequest->append(CCUtils::getStatusStringForStatus(record->status)).append(",");
    serverRequest->append(QString::number(record->consultationTypeId)).append(",");
    serverRequest->append(QString::number(record->physicianId)).append(",");
    serverRequest->append(QString::number(record->patientId)).append(",");
    serverRequest->append(QString::number(record->clinicId)).append(",");
    serverRequest->append(record->dateOfConsult.toString("yyyy-MM-dd")).append(",");
    serverRequest->append(record->dateOfConsult.toString("H:mm:ss")).append(",");
    serverRequest->append(record->reasonForConsultation).append(",");
    serverRequest->append(record->physicianNotes).append(",");
    serverRequest->append(record->additionalInformation).append(",");
    serverRequest->append(record->diagnosis).append(",");
    serverRequest->append("2,,\r\n\r\n");

    qDebug() << QString(*serverRequest);

    _socket->write(serverRequest);

    this->refreshConsultationRecords();

    return true;
}

QMap<int, CCConsultationRecord*> CCConsultationRecordControl::getConsultationRecordMap()
{
    if (_consultationRecordsMap.size() < 1)
    {
        this->refreshConsultationRecords();
    }

    return _consultationRecordsMap;
}

