#include "ccphysicianrecordcontrol.h"

CCPhysicianRecordControl* CCPhysicianRecordControlSingleton = 0;

CCPhysicianRecordControl::CCPhysicianRecordControl() :
    applyingFilter(false)
{
}

CCPhysicianRecordControl::~CCPhysicianRecordControl()
{

}

CCPhysicianRecordControl* CCPhysicianRecordControl::sharedPhysicianRecordControl()
{
    if (CCPhysicianRecordControlSingleton == 0)
    {
        CCPhysicianRecordControlSingleton = new CCPhysicianRecordControl();
    }
    return CCPhysicianRecordControlSingleton;
}

void CCPhysicianRecordControl::receiveServerData(QByteArray *received)
{
    QString constructorString = QString(*received);

    qDebug() << constructorString;

    if (constructorString.contains("SERVER_CONNECTION_CONFIRM"))
    {
        // Server connection completed
        return;
    }
    else if (constructorString.contains("MESSAGE_DUMP_END"))
    {
        // Clinic list completely returned
        _awaitingServerResponse = false;
        applyingFilter = false;
        _socket->disconnectFromServer();
    }
    else if (constructorString.contains("FILTER"))
    {
        applyingFilter = true;
        _filteredPhysicians.clear();
    }
    else if (applyingFilter)
    {
        // Parse through and set as QVector<int>
        QStringList parseString;
        parseString = constructorString.split(",");

        for (int i = 0; i < parseString.count(); i++)
        {
            _filteredPhysicians.append(parseString.at(i).toInt());
        }
    }
    else
    {
        CCPhysicianRecord* newRecord = new CCPhysicianRecord(constructorString);
        _physicianRecordsMap.insert(newRecord->physicianId, newRecord);
    }

}

CCPhysicianRecord* CCPhysicianRecordControl::getPhysicianRecordForId(int aPhysicianRecordId)
{
    if (_physicianRecordsMap.size() < 1 || (!_physicianRecordsMap.contains(aPhysicianRecordId)))
    {
        this->refreshPhysicianRecords();
    }

    if (_physicianRecordsMap.contains(aPhysicianRecordId))
    {
        return _physicianRecordsMap[aPhysicianRecordId];
    }

    return 0;
}

void CCPhysicianRecordControl::refreshPhysicianRecords()
{
    _physicianRecordsMap.clear();

    // call server and refresh map
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }

    _socket->connectToServer();
    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 86;
    serverRequest->append((char)requestVal);
    serverRequest->append("\r\n\r\n");
    _socket->write(serverRequest);

}

QMap<int, CCPhysicianRecord*> CCPhysicianRecordControl::getPhysicianRecordsMap(){
    if (_physicianRecordsMap.size() < 1)
    {
        refreshPhysicianRecords();
    }
    return _physicianRecordsMap;
}

QVector<int> CCPhysicianRecordControl::getPhysiciansFiltered(QString queriedFirstName, QString queriedLastName, QString queriedClinicName)
{
    // Update record on server
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }

    _socket->connectToServer();
    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 85;
    serverRequest->append((char)requestVal);
    serverRequest->append(queriedFirstName).append(",");
    serverRequest->append(queriedLastName).append(",");
    serverRequest->append(queriedClinicName).append(",");
    serverRequest->append("\r\n\r\n");

    _socket->write(serverRequest);

    return _filteredPhysicians;
}
