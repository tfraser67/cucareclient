#include "ccclinicrecord.h"

CCClinicRecord::CCClinicRecord(int aId, QString aClinicName) :
    clinicId(aId),
    clinicName(aClinicName)
{
}

CCClinicRecord::CCClinicRecord(QString constructorString)
{
    QStringList parseString;
    parseString = constructorString.split(",");

    clinicId = parseString.at(0).trimmed().toInt();
    clinicName = parseString.at(1).trimmed();
}

CCClinicRecord::~CCClinicRecord()
{
}
