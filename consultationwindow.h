#ifndef CONSULTATIONWINDOW_H
#define CONSULTATIONWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "ccpatientrecord.h"
#include "ccphysicianrecord.h"
#include "patientselectionwindow.h"
#include "physicianselectionwindow.h"
#include "ccclinicrecordcontrol.h"
#include "ccconsultationtypecontrol.h"
#include "ccconsultationrecord.h"

enum CCConsultationWindowType
{
    CCConsultationWindowTypeUnknown = 0,
    CCConsultationWindowTypeAdd,
    CCConsultationWindowTypeEdit,
    CCConsultationWindowTypeView
};

namespace Ui {
class ConsultationWindow;
}

class ConsultationWindow : public QMainWindow, public PhysicianSelectionWindowDelegate, public PatientSelectionWindowDelegate
{
    Q_OBJECT
    
public:
    explicit ConsultationWindow(CCConsultationWindowType windowType, CCConsultationRecord* record = 0, bool isFollowUp = false, QWidget *parent = 0);
    ~ConsultationWindow();
    void closeEvent(QCloseEvent *);

    void updatePatient(CCPatientRecord* patient);
    void updatePhysician(CCPhysicianRecord* physician);

    virtual void windowReturnedPhysicianList(QVector<int> physicianArray);
    virtual void windowReturnedPatientId(int patientId);

private slots:
    void on_patientSelectButton_clicked();
    void on_physicianSelectButton_clicked();
    void on_createConsultationButton_clicked();

    void clearPatientWindow();
    void clearPhysicianWindow();

    void on_cancelButton_clicked();

private:
    void populateClinics();
    void populateConsultationTypes();
    void setupViewWithRecord();

    Ui::ConsultationWindow *ui;
    CCPatientRecord* _patientRecord;
    CCPhysicianRecord* _physicianRecord;
    PatientSelectionWindow* _patientWindow;
    PhysicianSelectionWindow* _physicianWindow;
    CCConsultationWindowType _windowType;
    CCConsultationRecord* _consultationRecord;
    bool _isFollowUp;

    QVector<CCClinicRecord*> _clinicsList;
    QVector<CCConsultationType*> _consultationsTypeList;
};

#endif // CONSULTATIONWINDOW_H
