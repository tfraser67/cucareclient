#ifndef MANAGECONSULTATIONSWINDOW_H
#define MANAGECONSULTATIONSWINDOW_H

#include <QMainWindow>

#include "consultationwindow.h"

class CCConsultationRecord;

namespace Ui {
class ManageConsultationsWindow;
}

class ManageConsultationsWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ManageConsultationsWindow(QWidget *parent = 0);
    ~ManageConsultationsWindow();
    void refreshConsultationsTable();

    void closeEvent(QCloseEvent *);

private slots:
    bool _addConsultationToTable(CCConsultationRecord* record);
    void on_actionAdd_New_Consultation_triggered();
    void on_actionEdit_Selected_Consultation_triggered();
    void clearAddWindow();
    void clearEditWindow();
    void clearViewWindow();

    void on_actionRefresh_Consultations_triggered();

    void on_actionView_Consultation_triggered();

private:
    void populateTable();

    Ui::ManageConsultationsWindow *ui;
    ConsultationWindow* addWindow;
    ConsultationWindow* editWindow;
    ConsultationWindow* viewWindow;

    QVector<CCConsultationRecord*> consultationRecordList;
};

#endif // MANAGECONSULTATIONSWINDOW_H
