#ifndef CCPATIENTRECORDCONTROL_H
#define CCPATIENTRECORDCONTROL_H

#include "ccdatacontrol.h"
#include "ccpatientrecord.h"
#include <QMap>

class CCPatientRecordControl : public CCDataControl
{
public:
    CCPatientRecordControl();
    ~CCPatientRecordControl();
    static CCPatientRecordControl* sharedPatientRecordControl();

    void refreshPatientRecords();
    CCPatientRecord* getPatientRecordForId(int aPatientRecordId);
    QMap<int, CCPatientRecord*> getPatientRecordsMap();

    virtual void receiveServerData(QByteArray *received);

    QVector<int> getPatientsFiltered(bool useStatus, bool usePhysician, bool currentStatusOnly, CCConsultationRecordStatus status, bool consultingPhysicianOnly, QVector<int> physicianIds);

private:
    QMap<int, CCPatientRecord*> _patientRecordsMap;
    QVector<int> _filteredPatients;

    bool applyingFilter;
};

#endif // CCPATIENTRECORDCONTROL_H
