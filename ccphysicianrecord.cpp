#include "ccphysicianrecord.h"

CCPhysicianRecord::CCPhysicianRecord(int aId, QString aFirstName, QString aLastName, int aClinicId) :
    physicianId(aId),
    firstName(aFirstName),
    lastName(aLastName),
    clinicId(aClinicId)
{
}

CCPhysicianRecord::CCPhysicianRecord(QString constructorString)
{
    QStringList parseString;
    parseString = constructorString.split(",");

    physicianId = parseString.at(0).trimmed().toInt();
    firstName = parseString.at(1).trimmed();
    lastName = parseString.at(2).trimmed();
    clinicId = parseString.at(3).trimmed().toInt();
}

CCPhysicianRecord::~CCPhysicianRecord()
{

}
