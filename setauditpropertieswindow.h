#ifndef SETAUDITPROPERTIESWINDOW_H
#define SETAUDITPROPERTIESWINDOW_H

#include <QDialog>
#include "ccauditcontrol.h"

namespace Ui {
class SetAuditPropertiesWindow;
}

class SetAuditPropertiesWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit SetAuditPropertiesWindow(QWidget *parent = 0);
    ~SetAuditPropertiesWindow();
    
private slots:
    void on_buttonBox_accepted();

private:
    Ui::SetAuditPropertiesWindow *ui;
};

#endif // SETAUDITPROPERTIESWINDOW_H
