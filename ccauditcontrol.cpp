
#include "ccauditcontrol.h"
#include <QString>
#include <QStringList>

ccAuditControl* ccAuditControlSingleton = 0;

ccAuditControl::ccAuditControl()
{
}

ccAuditControl::~ccAuditControl()
{
}

ccAuditControl* ccAuditControl::sharedAuditControl()
{
        if (ccAuditControlSingleton == 0)
        {
            ccAuditControlSingleton = new ccAuditControl();
        }
        return ccAuditControlSingleton;
}

void ccAuditControl::receiveServerData(QByteArray *received){
    QString constructorString = QString(*received);


    if (constructorString.contains("SERVER_CONNECTION_CONFIRM") || constructorString.contains("Update_Confirm"))
    {
        // Server connection completed
        return;
    }
    else if (constructorString.contains("MESSAGE_DUMP_END"))
    {
        // Clinic list completely returned
        _awaitingServerResponse = false;
        _socket->disconnectFromServer();
    }
    else
    {
        qDebug() << constructorString;

        QStringList timeBits = constructorString.split(":");
        time = QTime(timeBits.at(0).toInt(), timeBits.at(1).toInt(), timeBits.at(2).toInt());
        qDebug() << time.toString();
    }
}

void ccAuditControl::saveAuditTime(QTime t){
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }

    _socket->connectToServer();
    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 78;
    serverRequest->append((char)requestVal);
    serverRequest->append("1900-01-01,");
    serverRequest->append(t.toString("H:mm:ss")).append(",");
    serverRequest->append("\r\n\r\n");
    _socket->write(serverRequest);
}

void ccAuditControl::refreshAuditControl(){
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }

    _socket->connectToServer();
    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 79;
    serverRequest->append((char)requestVal);
    serverRequest->append("\r\n\r\n");
    _socket->write(serverRequest);
}

QTime ccAuditControl::getAuditTime(){

    refreshAuditControl();
    return time;
}
