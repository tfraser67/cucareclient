#include "ccuserdatacontrol.h"
#include <QString>

CCUserDataControl* CCUserDataControlSingleton = 0;

CCUserDataControl::CCUserDataControl() :
    _userName(""),
    _priviledgeLevel(CCUserPriviledgeLevelNoAccess)
{
}

CCUserDataControl::~CCUserDataControl()
{
}

CCUserDataControl* CCUserDataControl::sharedUserDataControl()
{
    if (CCUserDataControlSingleton == 0)
    {
        CCUserDataControlSingleton = new CCUserDataControl();
    }

    return CCUserDataControlSingleton;
}

QString CCUserDataControl::getIpAddressOfServer()
{
    return _ipAddress;
}

bool CCUserDataControl::loginWithUserName(QString aUserName, QString aIpAddress)
{
    _userName = aUserName;
    _ipAddress = aIpAddress;

    bool loginSuccessful = false;

    // Attempt login
    loginSuccessful = true;

    if (loginSuccessful)
    {
        // Get priviledge level

        if (_userName == "physician")
        {
            _priviledgeLevel = CCUserPriviledgeLevelPhysician;
        }
        else if (_userName == "sysadmin")
        {
            _priviledgeLevel = CCUserPriviledgeLevelSysAdmin;
        }
        else
        {
            _priviledgeLevel = CCUserPriviledgeLevelAdminAssistant;
        }
    }
    else
    {
        // Clear user data
        this->_clearUserData();
    }

    return loginSuccessful;
}

CCUserPriviledgeLevel CCUserDataControl::getUserPriviledgeLevel()
{
    return _priviledgeLevel;
}

void CCUserDataControl::logoutUser()
{
    if (_userName != "")
    {
        // Log out user from server

        // Clear user information
        this->_clearUserData();
    }
}

void CCUserDataControl::_clearUserData()
{
    _userName = "";
    _priviledgeLevel = CCUserPriviledgeLevelNoAccess;
}
