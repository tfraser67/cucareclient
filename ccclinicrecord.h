#ifndef CCCLINICRECORD_H
#define CCCLINICRECORD_H

#include <QString>
#include <QStringList>

class CCClinicRecord
{
public:
    explicit CCClinicRecord(int aId, QString aClinicName);
    explicit CCClinicRecord(QString constructorString);
    ~CCClinicRecord();

    int clinicId;
    QString clinicName;
};

#endif // CCCLINICRECORD_H
