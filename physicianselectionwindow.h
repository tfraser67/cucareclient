
#ifndef PHYSICIANSELECTIONWINDOW_H
#define PHYSICIANSELECTIONWINDOW_H

#include <QMainWindow>
#include <QVector>
#include "ccphysicianrecord.h"

class PhysicianSelectionWindowDelegate
{
public:
    virtual void windowReturnedPhysicianList(QVector<int> physicianArray) = 0;
};

namespace Ui {
class PhysicianSelectionWindow;
}

class PhysicianSelectionWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PhysicianSelectionWindow(QWidget *parent = 0, PhysicianSelectionWindowDelegate* delegate = 0, bool multiselect = false);
    ~PhysicianSelectionWindow();

    void preselectPhysicians(QVector<int> preselectedPhysicians);

private slots:
    void on_selectPatientButton_clicked();

    void on_cancelButton_clicked();

    void on_clearFiltersButton_clicked();

    void on_applyFiltersButton_clicked();

private:
    Ui::PhysicianSelectionWindow *ui;
    QVector<CCPhysicianRecord*> physicianList;
    PhysicianSelectionWindowDelegate* _delegate;

    QVector<int> filteredPhysicians;

    void _addPhysicianToTable(CCPhysicianRecord* rec);
    void populateTable();
};

#endif // PHYSICIANSELECTIONWINDOW_H
