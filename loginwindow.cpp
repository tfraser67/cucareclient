#include "loginwindow.h"
#include "ui_loginwindow.h"
#include "ccuserdatacontrol.h"

LoginWindow::LoginWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);
}

LoginWindow::~LoginWindow()
{
    delete ui;
}

void LoginWindow::on_pushButton_clicked()
{
    QString username = this->ui->usernameField->text();

    QString ipAddress = this->ui->ipAddressField->text();
    if (CCUserDataControl::sharedUserDataControl()->loginWithUserName(username, ipAddress)){
        switch (CCUserDataControl::sharedUserDataControl()->getUserPriviledgeLevel()){
            case CCUserPriviledgeLevelPhysician:
                ShowPhysicianHome();
                break;
            case CCUserPriviledgeLevelAdminAssistant:
                ShowAdminAssistantHome();
                break;
            case CCUserPriviledgeLevelSysAdmin:
                ShowSystemAdminHome();
                break;
            default:
                break;
        }
        this->close();
    }
    else{
        QMessageBox* alert = new QMessageBox(QMessageBox::Critical, "Invalid Login", "Username/password combination not found", QMessageBox::Ok, this);
        //alert->setWindowFlags(Qt::WindowStaysOnTopHint);
        alert->show();
    }


}

void LoginWindow::ShowLoginScreen(){
    this->show();
    this->raise();
    this->activateWindow();
}

void LoginWindow::ShowPhysicianHome(){
    HomeWindow* home = new HomeWindow();
    home->show();
    home->raise();
    home->activateWindow();
}

void LoginWindow::ShowAdminAssistantHome(){
    HomeWindow* home = new HomeWindow();
    home->show();
    home->raise();
    home->activateWindow();
}
void LoginWindow::ShowSystemAdminHome(){
    SystemHomeWindow* home = new SystemHomeWindow();
    home->show();
    home->raise();
    home->activateWindow();
}
