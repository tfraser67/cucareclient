#ifndef VIEWCONSULTATIONWINDOW_H
#define VIEWCONSULTATIONWINDOW_H

#include <QMainWindow>
#include "ccconsultationrecord.h"

namespace Ui {
class ViewConsultationWindow;
}

class ViewConsultationWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ViewConsultationWindow(CCConsultationRecord* aRecord, QWidget *parent = 0);
    ~ViewConsultationWindow();
    void closeEvent(QCloseEvent *);

    CCConsultationRecord* consultationRecord;

private slots:
    void on_cancelButton_clicked();

private:
    void _setupViewWithRecord();
    Ui::ViewConsultationWindow *ui;
};

#endif // VIEWCONSULTATIONWINDOW_H
