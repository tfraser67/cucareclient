#ifndef MANAGEPATIENTSWINDOW_H
#define MANAGEPATIENTSWINDOW_H

#include <QMainWindow>
#include "physicianselectionwindow.h"
#include "ccpatientrecordcontrol.h"

namespace Ui {
class ManagePatientsWindow;
}

class ManagePatientsWindow : public QMainWindow, public PhysicianSelectionWindowDelegate
{
    Q_OBJECT
    
public:
    explicit ManagePatientsWindow(QWidget *parent = 0);
    ~ManagePatientsWindow();

    virtual void windowReturnedPhysicianList(QVector<int> physicianArray);
    void refreshPatientTable();

private slots:
    void on_filterPhysiciansButton_clicked();
    void on_applyFiltersButton_clicked();
    void on_clearFiltersButton_clicked();
    void clearPhysicianWindow();

private:
    bool _addPatientToTable(CCPatientRecord* record);

    Ui::ManagePatientsWindow *ui;
    PhysicianSelectionWindow *physicianWindow;

    QVector<int> physicianIdsFilter;
    QVector<int> filteredPatients;
    QVector<CCPatientRecord*> patientRecordList;
};

#endif // MANAGEPATIENTSWINDOW_H
