#ifndef CCUSERDATACONTROL_H
#define CCUSERDATACONTROL_H

#include <QString>

enum CCUserPriviledgeLevel
{
    CCUserPriviledgeLevelNoAccess = 0,
    CCUserPriviledgeLevelAdminAssistant = 1,
    CCUserPriviledgeLevelPhysician = 2,
    CCUserPriviledgeLevelSysAdmin = 3
};

class CCUserDataControl
{
public:
    CCUserDataControl();
    ~CCUserDataControl();
    static CCUserDataControl* sharedUserDataControl();

    bool loginWithUserName(QString aUserName, QString aIpAddress);
    CCUserPriviledgeLevel getUserPriviledgeLevel();
    void logoutUser();
    QString getIpAddressOfServer();

protected:
    void _clearUserData();
    QString _userName;
    CCUserPriviledgeLevel _priviledgeLevel;
    QString _ipAddress;
};

#endif // CCUSERDATACONTROL_H
