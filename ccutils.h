#ifndef CCUTILS_H
#define CCUTILS_H

#include <QString>
#include "ccconsultationrecord.h"

class CCUtils
{
public:
    static QString getStatusStringForStatus(CCConsultationRecordStatus status);
};

#endif // CCUTILS_H
