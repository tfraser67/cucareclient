#include "patientselectionwindow.h"
#include "ui_patientselectionwindow.h"
#include "addconsultationwindow.h"
#include "ccpatientrecordcontrol.h"

#include <QMessageBox>

PatientSelectionWindow::PatientSelectionWindow(QWidget *parent, PatientSelectionWindowDelegate* delegate) :
    QMainWindow(parent),
    _delegate(delegate),
    ui(new Ui::PatientSelectionWindow)
{
    ui->setupUi(this);

    ui->patientTableWidget->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    ui->patientTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->patientTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->patientTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    populateTable();
}

PatientSelectionWindow::~PatientSelectionWindow()
{
    delete ui;
}

void PatientSelectionWindow::populateTable()
{
    CCPatientRecordControl* patientControl = CCPatientRecordControl::sharedPatientRecordControl();
    QMap<int, CCPatientRecord*> patientMap = patientControl->getPatientRecordsMap();
    QMap<int, CCPatientRecord*>::iterator i;

    for (i = patientMap.begin(); i != patientMap.end(); ++i)
    {

        int rowIndex = ui->patientTableWidget->rowCount();
        ui->patientTableWidget->insertRow(rowIndex);

        CCPatientRecord* rec = i.value();
        ui->patientTableWidget->setItem(rowIndex, 0, new QTableWidgetItem(rec->lastName));
        ui->patientTableWidget->setItem(rowIndex, 1, new QTableWidgetItem(rec->firstName));
        ui->patientTableWidget->setItem(rowIndex, 2, new QTableWidgetItem(QString::number(rec->ohipNumber)));
        ui->patientTableWidget->setVerticalHeaderItem(rowIndex, new QTableWidgetItem(""));
        patientList.append(rec);
    }
}

void PatientSelectionWindow::on_selectPatientButton_clicked()
{

    QModelIndexList selectedRows = ui->patientTableWidget->selectionModel()->selectedRows();
    if (selectedRows.count() > 0)
    {
        int index = selectedRows.at(0).row();

        // Check that the index exists within the array and grab the consultations record

        if (index >= 0 && index < patientList.count())
        {
            // Get parent to refresh
            if (_delegate != NULL)
            {
                // Update with selected patient
                _delegate->windowReturnedPatientId(patientList.at(index)->ohipNumber);

                // Close window
                this->close();
            }
        }
    }
    else
    {
        // Message with "selected a row"
        QMessageBox* alert = new QMessageBox(QMessageBox::Information, "No patient was selected", "You must select a patient.", QMessageBox::Ok, this);
        alert->setWindowFlags(Qt::WindowStaysOnTopHint);
        alert->show();
    }
}

void PatientSelectionWindow::on_cancelButton_clicked()
{
    this->close();
}
