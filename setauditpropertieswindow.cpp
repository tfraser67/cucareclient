#include "setauditpropertieswindow.h"
#include "ui_setauditpropertieswindow.h"

SetAuditPropertiesWindow::SetAuditPropertiesWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetAuditPropertiesWindow)
{
    ui->setupUi(this);
    ui->timeEdit->setTime(ccAuditControl::sharedAuditControl()->getAuditTime());
}

SetAuditPropertiesWindow::~SetAuditPropertiesWindow()
{
    delete ui;
}

void SetAuditPropertiesWindow::on_buttonBox_accepted()
{
    ccAuditControl::sharedAuditControl()->saveAuditTime(ui->timeEdit->time());
}
