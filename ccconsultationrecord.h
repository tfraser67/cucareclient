#ifndef CCCONSULTATIONRECORD_H
#define CCCONSULTATIONRECORD_H

#include <QDateTime>
#include <QString>
#include <QStringList>
#include "ccconsultationtype.h"

enum CCConsultationRecordStatus
{
    CCConsultationRecordStatusPending = 0,
    CCConsultationRecordStatusComplete = 1,
    CCConsultationRecordStatusOverdue = 2
};

class CCConsultationRecord
{
public:
    explicit CCConsultationRecord(int aPatientId, int aPhysicianId, CCConsultationRecordStatus aStatus,
                                  int aClinicId, QDateTime aDate, int aConsultationTypeId, QString aReasonForConsult = "",
                                  QString aAdditionalInfo = "", QString aPhysicianNotes = "", QString aDiagnosis = "");
    explicit CCConsultationRecord(QString constructorString);
    ~CCConsultationRecord();

    int refNum;
    CCConsultationRecordStatus status;
    int physicianId;
    int patientId;
    int clinicId;
    QDateTime dateOfConsult;
    int consultationTypeId;
    QString reasonForConsultation;
    QString additionalInformation;
    QString physicianNotes;
    QString diagnosis;
};

#endif // CCCONSULTATIONRECORD_H
