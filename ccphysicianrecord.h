#ifndef CCPHYSICIANRECORD_H
#define CCPHYSICIANRECORD_H

#include <QString>
#include <QStringList>

class CCPhysicianRecord
{
public:
    CCPhysicianRecord(int aId, QString aFirstName, QString aLastName, int aClinicId = -1);
    CCPhysicianRecord(QString constructorString);
    ~CCPhysicianRecord();

    QString firstName;
    QString lastName;
    int physicianId;
    int clinicId;
};

#endif // CCPHYSICIANRECORD_H
