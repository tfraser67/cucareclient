#include "addconsultationwindow.h"
#include "ui_addconsultationwindow.h"
#include "manageconsultationswindow.h"
#include "patientselectionwindow.h"
#include "physicianselectionwindow.h"
#include "ccphysicianrecord.h"
#include "ccpatientrecord.h"
#include "ccclinicrecordcontrol.h"
#include "ccconsultationtypecontrol.h"
#include "ccconsultationrecordcontrol.h"

#include <QMap>
#include <QMessageBox>

AddConsultationWindow::AddConsultationWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AddConsultationWindow)
{
    patientWindow = NULL;
    physicianWindow = NULL;

    _patientRecord = NULL;
    _physicianRecord = NULL;

    ui->setupUi(this);

    ui->dateOfConsultDateTime->setTime(QTime::currentTime());
    populateClinics();
    populateConsultationTypes();
}

AddConsultationWindow::~AddConsultationWindow()
{
    delete ui;
}

void AddConsultationWindow::closeEvent(QCloseEvent *)
{
    const QObjectList list = this->children();
    for (int i = 0; i < list.count(); i++)
    {
        QWidget* item = qobject_cast<QWidget*>(list.at(i));
        if (item != NULL)
        {
            item->close();
        }
    }
}

void AddConsultationWindow::populateClinics()
{
    CCClinicRecordControl* clinRecControl = CCClinicRecordControl::sharedClinicRecordControl();

    ui->clinicComboBox->clear();
    clinicsList.clear();

    QMap<int, CCClinicRecord*> clinRecMap = clinRecControl->getClinicRecordsMap();
    clinicsList.reserve(clinRecMap.size());
    QMap<int, CCClinicRecord*>::iterator i;
    for (i = clinRecMap.begin(); i != clinRecMap.end(); ++i)
    {
        CCClinicRecord* record = (CCClinicRecord*)i.value();
        ui->clinicComboBox->addItem(record->clinicName);
        clinicsList.append(record);
    }
}

void AddConsultationWindow::populateConsultationTypes()
{
    CCConsultationTypeControl* typeControl = CCConsultationTypeControl::sharedConsultationTypeControl();

    ui->consultationTypeComboBox->clear();

    QMap<int, CCConsultationType*> typeMap = typeControl->getConsultationTypesMap();
    consultationsTypeList.reserve(typeMap.size());
    QMap<int, CCConsultationType*>::iterator i;
    for (i = typeMap.begin(); i != typeMap.end(); ++i)
    {
        CCConsultationType* type = (CCConsultationType*)i.value();
        ui->consultationTypeComboBox->addItem(type->typeName);
        consultationsTypeList.append(type);
    }
}

void AddConsultationWindow::on_patientSelectButton_clicked()
{
    // Pop up patient selector view
    if (patientWindow == NULL)
    {
        patientWindow = new PatientSelectionWindow(this);
        patientWindow->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(patientWindow, SIGNAL(destroyed()), this, SLOT(clearPatientWindow()));
    }

    if (patientWindow)
    {
        Qt::WindowFlags eFlags = patientWindow->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        patientWindow->setWindowFlags(eFlags);
        patientWindow->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        patientWindow->setWindowFlags(eFlags);
        patientWindow->show();
    }
}

void AddConsultationWindow::clearPatientWindow()
{
    patientWindow = NULL;
}

void AddConsultationWindow::on_physicianSelectButton_clicked()
{
    // Pop up physician selector view
    if (physicianWindow == NULL)
    {
        physicianWindow = new PhysicianSelectionWindow(this);
        physicianWindow->setAttribute(Qt::WA_DeleteOnClose, true);
        QObject::connect(physicianWindow, SIGNAL(destroyed()), this, SLOT(clearPhysicianWindow()));
    }

    if (physicianWindow)
    {
        Qt::WindowFlags eFlags = physicianWindow->windowFlags();
        eFlags |= Qt::WindowStaysOnTopHint;
        physicianWindow->setWindowFlags(eFlags);
        physicianWindow->show();
        eFlags &= ~Qt::WindowStaysOnTopHint;
        physicianWindow->setWindowFlags(eFlags);
        physicianWindow->show();
    }
}

void AddConsultationWindow::clearPhysicianWindow()
{
    physicianWindow = NULL;
}

void AddConsultationWindow::on_createConsultationButton_clicked()
{
    if (_patientRecord && _physicianRecord && ui->dateOfConsultDateTime->dateTime() > QDateTime::currentDateTime())
    {
        int patientId = _patientRecord->ohipNumber;
        int physicianId = _physicianRecord->physicianId;
        int clinicId = ((CCClinicRecord*)clinicsList.at(ui->clinicComboBox->currentIndex()))->clinicId;
        int consultationTypeId = ((CCConsultationType*)consultationsTypeList.at(ui->consultationTypeComboBox->currentIndex()))->id;

        QString reason,additional,notes,diagnosis;


        if(ui->reasonForConsultationTextEdit->toPlainText() == ""){
           reason = "None Given";
        }
        else{
           reason = ui->reasonForConsultationTextEdit->toPlainText();
        }
        if(ui->additionalInformationTextEdit->toPlainText() == ""){
            additional = "None Given";
        }
        else{
            additional = ui->additionalInformationTextEdit->toPlainText();
        }
        if(ui->physicianNotesTextEdit->toPlainText() == "" ){
            notes = "noneGiven";
        }
        else
        {
           notes =  ui->physicianNotesTextEdit->toPlainText();
        }
        if(ui->diagnosisTextEdit->toPlainText() == ""){
             diagnosis = "noneGiven";
        }
        else
        {
            diagnosis =  ui->physicianNotesTextEdit->toPlainText();
        }


        // Save record to database
        //CCConsultationRecordControl* recordControl = CCConsultationRecordControl::sharedConsultationRecordControl();

        CCConsultationRecord* record = new CCConsultationRecord(patientId, physicianId, CCConsultationRecordStatusPending, clinicId,
                                                                ui->dateOfConsultDateTime->dateTime(), consultationTypeId,
                                                                reason,
                                                                additional,
                                                                notes,
                                                                diagnosis);



        record->refNum =

        CCConsultationRecordControl::sharedConsultationRecordControl()->saveConsultationRecord(record);
        //recordControl->saveConsultationRecord(record);

        // Get parent to refresh
        ManageConsultationsWindow* mcw = qobject_cast<ManageConsultationsWindow*>(this->parent());
        if (mcw != NULL)
        {
            mcw->refreshConsultationsTable();
        }

        // Close window
        this->close();
    }
    else
    {
        // Error
        QMessageBox* alert = new QMessageBox(QMessageBox::Information, "Mandatory information incomplete", "You must select a patient, physician, clinic, consultation type and date for the consultation in order to add this consultation.", QMessageBox::Ok, this);
        alert->setWindowFlags(Qt::WindowStaysOnTopHint);
        alert->show();
    }
}

void AddConsultationWindow::updatePatient(CCPatientRecord* patient)
{
    _patientRecord = patient;

    QString patientText = "Patient: ";
    ui->patientLabel->setText(patientText.append(_patientRecord->firstName).append(" ").append(_patientRecord->lastName));
}

void AddConsultationWindow::updatePhysician(CCPhysicianRecord* physician)
{
    _physicianRecord = physician;

    QString physicianText = "Physician: ";
    ui->physicianLabel->setText(physicianText.append(_physicianRecord->firstName).append(" ").append(_physicianRecord->lastName));
}

void AddConsultationWindow::on_cancelButton_clicked()
{
    this->close();
}
