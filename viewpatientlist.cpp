#include "viewpatientlist.h"
#include "ui_viewpatientlist.h"

ViewPatientList::ViewPatientList(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewPatientList)
{
    ui->setupUi(this);
}

ViewPatientList::~ViewPatientList()
{
    delete ui;
}
