#include "ccconsultationtype.h"

CCConsultationType::CCConsultationType(int aId, QString aTypeName) :
    id(aId),
    typeName(aTypeName)
{
}

CCConsultationType::CCConsultationType(QString constructorString)
{
   QStringList parseString =  constructorString.split(",");
     id = parseString.at(0).trimmed().toInt();
     typeName = parseString.at(1).trimmed();
}

CCConsultationType::~CCConsultationType()
{

}
