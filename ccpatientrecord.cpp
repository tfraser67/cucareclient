#include "ccpatientrecord.h"
#include <QDebug>

CCPatientRecord::CCPatientRecord(QString aFirstName, QString aLastName, int aOhipNumber, QDate aDateOfBirth,
                                 int aPhysicianId, int aAreaCode, int aStationCode, int aDirectoryCode,
                                 QString aAddress1, QString aAddress2, QString aProvince, QString aCountry,
                                 QString aPostalCode, CCConsultationRecordStatus aPatientStatus) :
    firstName(aFirstName),
    lastName(aLastName),
    ohipNumber(aOhipNumber),
    physicianId(aPhysicianId),
    dateOfBirth(aDateOfBirth),
    areaCode(aAreaCode),
    stationCode(aStationCode),
    directoryCode(aDirectoryCode),
    address1(aAddress1),
    address2(aAddress2),
    province(aProvince),
    country(aCountry),
    postalCode(aPostalCode),
    patientStatus(aPatientStatus)
{
}

CCPatientRecord::CCPatientRecord(QString constructorString)
{
    QStringList parseString;
    parseString = constructorString.split(",");

    //Must change date and phone number

    ohipNumber = parseString.at(0).trimmed().toInt();
    firstName = parseString.at(1).trimmed();
    lastName = parseString.at(2).trimmed();
    //email = 3
    address1 = parseString.at(4).trimmed();
    address2 = parseString.at(4).trimmed();
    //phone = 5
    areaCode = 666;
    stationCode = 666;
    directoryCode = 6666;
    postalCode = parseString.at(6).trimmed();
    province = parseString.at(7).trimmed();
    country = parseString.at(8).trimmed();
    QDate date = QDate::fromString(parseString.at(9).trimmed(),"yyyy-mm-dd");
    dateOfBirth = date;
    physicianId = parseString.at(10).trimmed().toInt();

    if (parseString.count() > 12)
    {
        QString theStatus = parseString.at(12).trimmed();
        if (theStatus == "pending")
        {
            patientStatus = CCConsultationRecordStatusPending;
        }
        else if (theStatus == "overdue")
        {
            patientStatus = CCConsultationRecordStatusOverdue;
        }
        else
        {
            patientStatus = CCConsultationRecordStatusComplete;
        }
    }
}

CCPatientRecord::~CCPatientRecord()
{

}
