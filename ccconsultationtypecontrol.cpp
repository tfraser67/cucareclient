#include "ccconsultationtypecontrol.h"

CCConsultationTypeControl* CCConsultationTypeControlSingleton = 0;

CCConsultationTypeControl::CCConsultationTypeControl()
{
}

CCConsultationTypeControl::~CCConsultationTypeControl()
{
}

CCConsultationTypeControl* CCConsultationTypeControl::sharedConsultationTypeControl()
{
    if (CCConsultationTypeControlSingleton == 0)
    {
        CCConsultationTypeControlSingleton = new CCConsultationTypeControl();
    }

    return CCConsultationTypeControlSingleton;
}

void CCConsultationTypeControl::receiveServerData(QByteArray *received)
{
    QString constructorString = QString(*received);

    //qDebug() << constructorString;

    if (constructorString.contains("SERVER_CONNECTION_CONFIRM"))
    {
        // Server connection completed
        return;
    }
    else if (constructorString.contains("MESSAGE_DUMP_END"))
    {
        // Clinic list completely returned
        _awaitingServerResponse = false;
        _socket->disconnectFromServer();
    }
    else
    {
        CCConsultationType* newRecord = new CCConsultationType(constructorString);
        _consultationTypesMap.insert(newRecord->id, newRecord);
    }
}

QString CCConsultationTypeControl::getConsultationTypeNameForId(int aConsultationTypeId)
{
    if (_consultationTypesMap.size() < 1 || (!_consultationTypesMap.contains(aConsultationTypeId)))
    {
        this->refreshConsultationTypes();
    }

    if (_consultationTypesMap.contains(aConsultationTypeId))
    {
        return _consultationTypesMap[aConsultationTypeId]->typeName;
    }

    return "Unknown Consultation Type";
}

void CCConsultationTypeControl::refreshConsultationTypes()
{
    _consultationTypesMap.clear();

    // call server and refresh map
    if (_socket == NULL)
    {
        _socket = new clientSocket(this);
    }

    _socket->connectToServer();
    _awaitingServerResponse = true;
    QByteArray* serverRequest = new QByteArray();
    int requestVal = 76;
    serverRequest->append((char)requestVal);
    serverRequest->append("\r\n\r\n");
    _socket->write(serverRequest);

}

QMap<int, CCConsultationType*> CCConsultationTypeControl::getConsultationTypesMap(){
    if (_consultationTypesMap.size() < 1)
    {
        refreshConsultationTypes();
    }
    return _consultationTypesMap;
}
