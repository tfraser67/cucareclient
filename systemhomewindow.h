#ifndef SYSTEMHOMEWINDOW_H
#define SYSTEMHOMEWINDOW_H

#include <QMainWindow>
#include "loginwindow.h"
#include "setauditpropertieswindow.h"

namespace Ui {
class SystemHomeWindow;
}

class SystemHomeWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit SystemHomeWindow(QWidget *parent = 0);
    ~SystemHomeWindow();
    
private slots:
    void on_logoutButton_clicked();

    void on_auditPropertiesButton_clicked();

private:
    Ui::SystemHomeWindow *ui;
};

#endif // SYSTEMHOMEWINDOW_H
